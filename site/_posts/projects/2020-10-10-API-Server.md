---
layout: project
permalink: /:title/
category: projects

meta:
  keywords: "Flask, API, Docker"

project:
  title: "API Server"
  type: "Python"
  url: "https://github.com/Cyb3r-Jak3/api_server"
  logo: "/assets/images/projects/TBD/python.svg"
  status: "Stable"
  version: "v1"
  year: "2020"
  tech: "Docker, Flask, Python"
  

images:
  - image:
    url: "/assets/images/projects/TBD/python.svg"
    alt: "Python Logo"
---
I whipped up a Python API server that I can now attach to my websites to provide backend processing with materials and processing. Right now the only endpoint is for getting my resume encrypted with a user submitted private key. I am think about adding more, but for more update to date status checkout the repo on [GitHub](https://github.com/Cyb3r-Jak3/api_server)
