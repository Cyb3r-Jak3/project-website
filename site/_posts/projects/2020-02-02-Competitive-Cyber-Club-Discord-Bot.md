---
layout: project
permalink: /:title/
category: projects

meta:
  keywords: "Discord, Competitive Cyber Club"

project:
  title: "Discord Bot"
  type: "Python"
  url: "https://github.com/Competitive-Cyber-Clubs/Discord-Bot"
  logo: "https://raw.githubusercontent.com/Competitive-Cyber-Clubs/competitivecyberclubs.github.io/master/public/images/logo.png"
  status: "Stable"
  version: "v0.1.0"
  year: "2019-"
  tech: "Python, async, postgresql, discord.py, heroku"

images:
  - image:
    url: "/assets/images/projects/Discord-Bot/demo.gif"
    alt: "Demo GIF of interaction with Discord Bot"
---
This discord bot was written for [Competitive Cyber Club](https://competitivecyberclubs.org/) to assist with the management of the Discord server. It is my biggest and longest project to date. While the bot was highly customized for the specific server, parts of code can be reused to create your own bot.
