---
layout: project
permalink: /:title/
category: projects

meta:
  keywords: "Python, EXIF"

project:
  title: "MetaStalk"
  type: "Python"
  url: "https://gitlab.com/Cyb3r-Jak3/metastalk"
  logo: "/assets/images/projects/MetaStalk/logo.png"
  status: "Stable"
  version: "<a href='https://pypi.org/project/MetaStalk/'> <img src='https://img.shields.io/pypi/v/metastalk' style='width:100px;height:20px;' alt='pypi logo showing version'></a>"
  year: "2020-"
  tech: "Python, exifread"
  

images:
  - image:
    url: "/assets/images/projects/MetaStalk/demo1.png"
    alt: "Demo Photo 1 of MetaStalk"
  - image:
    url: "/assets/images/projects/MetaStalk/demo2.png"
    alt: "Demo Photo 2"
---
MetaStalk is a Python program that parses images and extracts exif data from them to create visualizes. It uses plotly for the graph visilzation and dash to run the webpage, uses exifreader. It is available as package from pypi and the source code is available on my [GitLab](https://gitlab.com/Cyb3r-Jak3/metastalk)
