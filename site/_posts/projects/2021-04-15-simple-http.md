---
layout: project
permalink: /:title/
category: projects

meta:
  keywords: "golang http mux docker"

project:
  title: "Simple HTTP Server"
  type: "GoLang"
  url: "https://github.com/Cyb3r-Jak3/simple_http_server"
  logo: "https://cdn.worldvectorlogo.com/logos/golang-gopher.svg"
  status: "First version completed"
  version: "v1.2.0"
  year: "2021"
  tech: "GoLang HTTP Docker"
  

images:
  - image:
    url: "/assets/images/projects/simple-http-server/filler-code.png"
    alt: "Under Construction"
---

Simple HTTP server is a program that I wrote in GoLang. The main goal of this program was to learn about handling web requests with GoLang. I got my inspiration from [httpbin.org](https://httpbin.org). It will generate random data to complete the requests and has optional input.
