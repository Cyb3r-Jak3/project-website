---
layout: project
permalink: /:title/
category: projects

meta:
  keywords: "DDNS, Python, REST API"

project:
  title: "Python DDNS"
  type: "Python"
  url: "https://gitlab.com/Cyb3r-Jak3/python-ddns"
  logo: "/assets/images/projects/TBD/logo.png"
  status: "Under Development"
  version: "<a href='https://pypi.org/project/python-ddns/'><img alt='PyPI' src='https://img.shields.io/pypi/v/python-ddns' style='width:100px;height:20px;'></a>"
  year: "2019-"
  tech: "Python, REST API"
  

images:
  - image:
    url: "/assets/images/projects/TBD/logo.png"
    alt: "Under Construction"
  - image:
    url: "/assets/images/projects/TBD/Blank-Photo.jpg"
    alt: "Place holder"
---
Python DDNS ("pddns") is a dynamic domain name that it is written in Python. It will take the public IP address of the client system it is running on and create or update records. It currently supports Cloudflare and Hurricane Electric for DNS providers. Future updates will bring support for more providers, IPv6 support as well as the ability to use the public facing IP of the server for servers that are behind firewalls.
