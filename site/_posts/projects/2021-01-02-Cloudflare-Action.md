---
layout: project
permalink: /:title/
category: projects

meta:
  keywords: "Github Action, Cloudflare, Docker"

project:
  title: "Clear Cloudflare Cache"
  type: "Github Actions"
  url: "https://github.com/Cyb3r-Jak3/Clear-Cloudflare-Cache"
  logo: "/assets/images/projects/TBD/logo.png"
  status: "Stable"
  version: "0.0.1"
  year: "2021"
  tech: "Docker, Github Actions, Cloudflare"
  

images:
  - image:
    url: "/assets/images/projects/TBD/logo.png"
    alt: "Under Construction"
---
I created a Github action that will allow users to easiler clear their github cache for a site. There were a couple of actions that had compelted this before but one had not been updated in over a year and was not really usable with the new Github Actions worflow. This was a quick project as I was able to copy most of the layout and templates from my other action html5validator.