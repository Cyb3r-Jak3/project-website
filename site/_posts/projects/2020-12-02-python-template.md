---
layout: project
permalink: /:title/
category: projects

meta:
  keywords: "python github repo-template"

project:
  title: "Python Template Repo"
  type: "Github Python"
  url: "https://github.com/Cyb3r-Jak3/python_template_repo"
  logo: "/assets/images/projects/TBD/python.svg"
  status: ""
  version: "v1.0.0"
  year: "2020-"
  tech: "Python Github Actions"
  

images:
  - image:
    url: "/assets/images/projects/repo-template/filler.png"
    alt: "Snips of the repo"
---
Python template repo is a template repo that I made to easily create new Github repos for Python Libraries. It has a template setup.py, readme, test and github actions. It will auto build and deploy new tags on the master/main branch. I have found it is a good way to start new Python projects.
